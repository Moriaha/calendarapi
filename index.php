<?php

require __DIR__ . '/vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secrets.json');
$client->addScope('https://www.googleapis.com/auth/calendar');

if (isset($_SESSION['access_token']) && $_SESSION['access_token'] &&  !empty($_POST['startEvent']) && !empty($_POST['endEvent'])) {
  
  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

  $event = new Google_Service_Calendar_Event(array(
  'summary' => 'test event 2017',
  'location' => 'Jerusalem',
  'description' => 'Testing the google calendar API',
  'start' => array(
    'dateTime' => $_POST['startEvent'] . ':00-07:00', //'2017-02-28T05:00:00-07:00', //14:00 israel //9 hours after if the form hour is 06:00 the calendar houe is 15:00
    'timeZone' => 'America/Los_Angeles',
  ),
  'end' => array(
    'dateTime' => $_POST['endEvent'] . ':00-07:00', //'2017-02-28T06:00:00-07:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'recurrence' => array(
    'RRULE:FREQ=DAILY;COUNT=1'
  ),
  'attendees' => array(
    array('email' => 'lpage@example.com'),
    array('email' => 'sbrin@example.com'),
  ),
  'reminders' => array(
    'useDefault' => FALSE,
    'overrides' => array(
      array('method' => 'email', 'minutes' => 24 * 60),
      array('method' => 'popup', 'minutes' => 10),
    ),
  ),
));
  
$calendarId = 'primary';
$event = $service->events->insert($calendarId, $event);
printf('Event created: %s\n', $event->htmlLink);
  
} elseif (!isset($_SESSION['access_token']) || !$_SESSION['access_token']) {
  $redirect_uri =  'http://moriaha.myweb.jce.ac.il/calendar/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));

}

else {?> 

	<form role="form" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<header>
				<h2>New Event</h2>
			</header>
				
				<div class="form-group">
					<label for="startEvent">Start Event:</label>
					<input type="datetime-local" name="startEvent" class="form-control" id="startEvent"> <!--onfocus="myfocusfunction(this)" onblur="myblurfunction(this)"-->
				</div>
				
				<div class="form-group">
					<label for="endEvent">End Event:</label>
					<input type="datetime-local" name="endEvent" class="form-control" id="endEvent"> <!--onfocus="myfocusfunction(this)" onblur="myblurfunction(this)"-->
				</div>

			<button type="submit" class="btn btn-default">Add Event</button>
		
		</form>
<?php } ?>




  